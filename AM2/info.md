# Anàlisi matemàtica

- Carlos Garía Gómez
- **Despatx:** 233
- carlos.garciag@urv.cat

## Avaluació

- **30%** $\rightarrow$ Parcial
- **40%** $\rightarrow$ Examen global
- **30%** $\rightarrow$ WIMS

## Continguts

### Funcions amb Varies Variables (50%)

- Límits
- Derivades

### Equacions Diferencials - EDOs (30%)

### Integració múltiple (20%)
