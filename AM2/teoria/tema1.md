
# Tema 1: Espais Vectorials Euclidians

- Un espai euclidià és un espai afi en el que hem definit un producte esalar.

- La norma d'un espai euclidià és:

$$
||u|| = \sqrt{u\cdot v}
$$

## Vector unitari

- Un vector $u$ de $E$ és unitari si $||u|| = 1$
- Qualsevol vector $v$ de $E$ es pot convertir en unitari de la següent forma:

$$
||u|| = \dfrac{v}{||v||}
$$

- Donats dos vectors $u$ i $v$ d'un espai vectorial $E$ es verifica:

$$
| u \cdot v | \leq ||u|| \cdot ||v||
$$

- La **desigualtat triangular de Minkowski** ens diu:

$$
|| u + v || \leq ||u|| + ||v||
$$

- Angles:

$$
\cos\theta = \dfrac{u\cdot v}{||u||\;||v||}
$$


## Producte vectorial

$$W=u \times v = 
\begin{pmatrix}
i & j & k \\
u_1 & u_2 & u_3 \\ 
v_1 & v_2 & v_3 
\end{pmatrix}$$

## Coordenades polars

$$
x = r\cdot\cos\theta
$$
$$
y = r\cdot\sin\theta
$$

## Funcions de Varies Variables

$$
f: A\subset \R^n\rightarrow B\subset\R^n
$$

$$
x \rightarrow f\left(x\right) = y
$$
