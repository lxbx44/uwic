#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


int generar_valor(int min, int max) {   
    return min + rand() % (max - min + 1);
}

void mostrar_dades(int *d, int n_files, int n_cols) {
    for (int i = 0; i < n_files; i++) {
        for (int j = 0; j < n_cols; j++) {
            printf("%d\t", d[i * n_cols + j]);
        }
        printf("\n");
    }
}

bool desar_taula_fitxer_t(int *d, int n_files, int n_cols, char nom[]) {
    bool r = false;
    FILE *f;

    f = fopen(nom, "w");

    if (f != NULL) {
        r = true;
    
        for (int i = 0; i < n_files; i++) {
            for (int j = 0; j < n_cols; j++) {
                fprintf(f, "%d\t", d[i * n_cols + j]);
            }
            fprintf(f, "\n");
        }
    
        fclose(f);
    }

    return r;
}


bool desar_taula_fitxer_b(int *d, int n_files, int n_cols, char nom[]) {
    bool r = false;
    FILE *f;

    f = fopen(nom, "wb");

    if (f != NULL) {
        r = true;
    
        for (int i = 0; i < n_files; i++) {
            for (int j = 0; j < n_cols; j++) {
                fwrite(&d[i * n_cols + j], sizeof(int), 1, f);
            }
            fwrite("\n", sizeof("\n"), 1, f);
        }
    
    fclose(f);
    }

    return r;
}
