#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include "funcions.h"

#define MIN_FC 1
#define MAX_FC 10

int main(int argc, char *argv[]) {
    int nf, nc;
    int *dades;

    bool hi_ha_errors = false;

    if (argc == 3) {
        nf = atoi(argv[1]);
        nc = atoi(argv[2]);

        if (nf <= MIN_FC && nf >= MAX_FC && nc <= MIN_FC && nc >= MAX_FC) {
            printf("Els valors han d'estar entre %d i %d\n", MIN_FC, MAX_FC);
            hi_ha_errors = true;
        }
    }
    else {
        printf("El nombre de parametres no és correcte!\n");
        hi_ha_errors = true;
    }

    if (!hi_ha_errors) {
        
        dades = (int*) malloc(nf * nc * sizeof(int));

        if (dades == NULL)
        {
            printf("Sense espai per la taula!\n");
            hi_ha_errors = true;
        }
    }

    if (!hi_ha_errors) {
        srand(time(0));

        int total = nf * nc;

        int x;
        for (x = 0; x <= total; ++x) {
            dades[x] = generar_valor(-99999, 99999);
        }

        mostrar_dades(dades, nf, nc);
    }

    if (!hi_ha_errors) {
        if (desar_taula_fitxer_t(dades, nf, nc, "fitxer_t.txt")) {
            printf("Dades desades correctament en text.\n");
        }
        else {
            printf("Error en desar les dades.\n");
        }
    }

    if (!hi_ha_errors) {
        if (desar_taula_fitxer_b(dades, nf, nc, "fitxer_b.txt")) {
            printf("Dades desades correctament en binari.\n");
        }
        else {
            printf("Error en desar les dades\n");
        }
    }

    if (!hi_ha_errors) {
        free(dades);
    }

    printf("Adeu-siau!\n");

    return 0;
}


