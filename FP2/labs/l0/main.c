#include <stdio.h>
#include <stdlib.h>

#include <stdbool.h>
#include <time.h>

int main() {
    bool ok = false;
    int input;

    while (!ok) {
        printf("Escriu un nombre entre el 10 i el 100:\n");
        scanf("%d", &input);

        if (input >= 10 && input <= 100) {
            ok = true;
        }
    }

    srand(time(NULL));
    int aleatori = rand() % input + 1;

    int total_intents = 10;
    bool fi = false;

    while (!fi) {
        if (total_intents == 1) {
            printf("Has perdut el joc, tens 0 intents");
            fi = true;
        }

        printf("Entra un nombre entre el 1 i el %d\nTens %d intents\n", input, total_intents);
        int tria;
        scanf("%d", &tria);

        if (tria == aleatori) {
            printf("Molt be! %d era el nombre correcte!\n", aleatori);
            fi = true;
        } else if (tria > aleatori) {
            printf("El %d es mes gran que el nombre a triar\n", tria);
        } else if (tria < aleatori) {
            printf("El %d es mes petit que el nombre a triar\n", tria);
        }

        total_intents--;
    }

    return 0;
}
