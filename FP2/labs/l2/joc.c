// Una solucio a la practiqueta del laboratori L2, que es desenvolupa en dues sessions

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#include "joc.h"

// Inicialitza la partida
void iniciar_joc(joc_t *j, char *argv[])
{
    srand(time(NULL));
    j->guanyador = -1;  // El joc no ha acabat :-)
    j->torn = rand() % 2;      // Jugador inicial
    j->punts[0] = 0;    // Punts inicials de l'ordinador
    j->punts[1] = 0;    // Punts inicials de la persona
    strcpy(j->nom, argv[1]);    // Nom del jugador/a

    // Inicialitzar el tauler
    for (int i = 0; i < MIDA_TAULER; i++)
    {
        j->tauler[i] = '-';
    }
}

bool hi_ha_joc(joc_t *j)
{
    bool f = false;
    if (j->guanyador == -1) {
        f = true;
    }
    return f;
}



void missatge_benvinguda(joc_t *j)
{
    printf("Hola %s!\n", j->nom);
    printf("Benvingut/da al joc 'sumar 15'!\n");
}


void missatge_comiat(joc_t *j)
{
    if (j->guanyador == 0)
    {
        printf("Ho sento %s! Ha guanyat l'ordinador!\n", j->nom);
    }
    else
    {
        printf("Enhorabona %s! Has guanyat a l'ordinador!\n", j->nom);
    }
    printf("Fins aviat!\n");
}


void mostrar_joc(joc_t *j)
{
    printf("\n");
    printf("Punts P (%s) %d - Punts O (ordinador) %d\n", j->nom, j->punts[1], j->punts[0]);

    for (int i = 0; i < MIDA_TAULER; i++)
    {
        printf("%d\t", i + 1);
    }

    printf("\n");
    for (int i = 0; i < MIDA_TAULER; i++)
    {
        printf("%c\t", j->tauler[i]);
    }
    printf("\n");
}

// Interaccio amb usuari partida
int fer_jugada(joc_t *j)
{
    int posicio;

    switch(j->torn)
    {
        // Juga l'ordinador
        case 0:
            printf("Toca jugar a l'ordinador. ");
            
                do {
                    srand(time(NULL));
                    posicio = 1 + rand() % 10;
                }
                while (j->tauler[posicio-1] != '-');

            printf("Ha agafat la posicio %d\n", posicio);
            
            break;

        // Juga la persona
        case 1:
            printf("Toca jugar a %s.\n", j->nom);


            do {
                printf("Quin numero agafes?\n");
                scanf("%d", &posicio);

                if (j->tauler[posicio-1] != '-') {
                    printf("Nombre ja utilitzat\n");
                }
            }
            while (j->tauler[posicio-1] != '-');

            break;
    }
   return posicio;
}

int toggle_torn(joc_t *j) {
    if (j->torn == 0) {
        return 1;
    }
    return 0;
}

// Actualitza la partida
void actualitzar_joc(joc_t *j, int posicio)
{
    // Sumem els punts al jugador
    j->punts[j->torn] += posicio;

    // Actualitzem el tauler
    switch (j->torn) {
        case 0:
            j->tauler[posicio-1] = 'O';
            break;
        case 1:
            j->tauler[posicio-1] = 'P';
            break;
    }

    // Mirar si hi ha guanyador
    // Si no, actualitzem el torn
    if (j->punts[j->torn] == 15) {
        j->guanyador = j->torn;
    } else if (j->punts[j->torn] > 15) {
        j->guanyador = toggle_torn(j);
    } else {
        j->torn = toggle_torn(j);
    }
}

