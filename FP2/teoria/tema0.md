
# TEMA 0: C per a adults

## Increments

```c
int x = 0;

x = x + 1;
x += 1;
x++;
```

## Enum types

```c
typedef enum {
    dilluns,
    dimarts,
    dimecres,
    dijous,
    divendres,
    dissabte,
    diumenge
} dia_t;

dia_t d = dimecres;

d++; // dijous
```

## argc and argv

```c
#include <stdio.h>;

int main(int argc, char *argv[]) {
    // argc -> total number of parameters
    // argv -> vectors with all the parameters

    // if there are two parms (filename, smth)
    if (argc == 1) {
        // Print Hello <argument>
        printf("Hello %s", argv[1]);
        return 0;
    }

    return 1;
}
```

## Malloc

```c
#include <stdio.h>

int main() {
    int *dades; /* Punter a les dades */
    int n_dades; /* Quantes dades */
    ...
        dades = malloc(n_dades * sizeof(int));
    if (dades == NULL) {
        printf("Les dades no hi caben\n");
        // ...
    }

    // Un cop usades les dades
    free(dades);

    return 0;
}
```

## Fitxers binaris

```c
#include <stdio.h>

int main() {
    int num = 2;
    FILE *f = fopen(filename, "wb");

    if (file != NULL) {
        fwrite(&num, sizeof(num), 1, f);
        fclose(f);
    }
}
```
