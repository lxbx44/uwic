
# FONAMENTS DE COMPUTADORS 1

## Info

- **Pere Millán** 
- **Masgistral** $\rightarrow$ 205

### Lab

- Simeó Reig
- simeo.reig@urv.cat

## Avaluació

- Nota minima per aprobar $\rightarrow$ 4

- Test teoria 
    - **35%:** Temes 1, 2
    - **25%:** Tema 3
- Lab/pràctiques
    - **25%:** Arm
    - **15%:** Cric Dig
- **+1 punt:** Posa't a prova
