/*---------------------------------------------------------------------------
|   Description: a program to check whether a positive number is prime
|	Version V1 (native MINGW32): evaluate a list of test cases, showing
|				the tested number, expected result and actual result for
|				each case, and finally counting how many tests have provided
|				the right output value, as well as the total number of tests
|	IMPORTANT NOTE: the function is_prime() is bugged on purpose, since it is
|				intended as an example of a debugging process!
|----------------------------------------------------------------------------
|	Author: Santiago Romani (DEIM, URV)
|	Date:   May/2020
| ---------------------------------------------------------------------------*/

#include <stdio.h>			/* extern declarations of libc functions: printf()*/
#include "isprime.h"		/* extern declaration of the is_prime() function */

/* type definition of the structured record that holds the test case values */
typedef struct {
	unsigned int num;		/* the input number (parameter) for is_prime() */
	int xprime;			/* the expected output value (result) from is_prime()*/
} test_struct;

/* the list of test case values; the letter labels (such as '_A_') at the
	beginning of each comment indicates a specific type of test; when one
	type appears for the first time in the list, the letter is inbetween
	two underscores ('_A_'), otherwise, the letter is after two underscores
	('__A'); theoretically, one could remove the repeated cases and the test
	set would be either confident, although sometimes there exist unexpected
	bugs on apparently similar types of tests */
test_struct test_case[] =
{{0, 0},			/* _A_	special case num < 2 	(result = 0) */
 {1, 0},			/* __A	special case num < 2 	(result = 0) */
 {2, 1},			/* _B_	special case num = 2 	(result = 1) */
 {3, 1},			/* __B	special case num = 3 	(result = 1) */
 {4, 0},			/* _C_	lowest even num > 2		(n % 2 == 0) */
 {5, 1},			/* _D_	first prime num > 3		(0 ite, 3*3 > 5) */
 {6, 0},			/* __C	another even number		(n % 2 == 0) */
 {7, 1},			/* __D	next prime 				(0 ite, 3*3 > 7) */
 {9, 0},			/* _E_	first odd not prime		(0 ite, 9 % 3 == 0, 3*3 == 9) */
 {11, 1},			/* _G_	next prime				(1 ite, 5*5 > 11) */
 {15, 0},			/* _F_	second odd not prime	(0 ite, 15 % 3 == 0, 3*3 < 15) */
 {21, 0},			/* __F	next not prime			(0 ite, 21 % 3 == 0, 3*3 < 21) */
 {25, 0},			/* _H_	next square, not prime	(1 ite, 25 % 5 == 0, 5*5 == 25) */
 {29, 1},			/* __G	another prime			(2 ite, 7*7 > 29) */
 {35, 0},			/* _I_	next not prime			(1 ite, 35 % 5 == 0, 5*5 < 35) */
 {49, 0},			/* __H	another square			(2 ite, 49 % 7 == 0, 7*7 == 49) */
 {83, 1},			/* __G	another prime			(4 ite, 11*11 > 83) */
 {91, 0},			/* __I	another not prime		(2 ite, 91 % 7 == 0, 7*7 < 91) */
 {127, 1},			/* __G	another prime			(5 ite, 13*13 > 127) */
 {253, 0},			/* __I	another not prime		(4 ite, 253 % 11 == 0, 11*11 < 253) */
 {383, 1},			/* __G	another prime			(9 ite, 21*21 > 383) */
 {625, 0},			/* __I	another not prime		(1 ite, 625 % 5 == 0, 5*5 < 625) */
 {10007, 1},		/* __G	first prime > 10000		(49 ite, 101*101 > 10007) */
 {10117, 0},		/* __I	another not prime		(32 ite, 10117 % 67 == 0, 67*67 < 10117) */
 {727609, 0},		/* __H	high square				(425 ite, 853*853 == 727609) */
 {1000003, 1},		/* __G	first prime > 1000000	(499 ite, 1001*1001 > 1000003) */
 {10156969, 0},		/* __H	high square				(1592 ite, 3187*3187 == 10156969) */
 {215543101, 0},	/* __I	another big not prime	(505 ite, 1013*1013 < 215543101) */
 {4292739361, 0},	/* __H	highest square			(32758 ite, 65519*65519 == 4292739361) */
 {4294967291, 1},	/* __G	highest prime			(32767 ite, 65537*65537 > 4294967291) */
 {4294967293, 0},	/* __I	huge not prime			(4619 ite, 9241*9241 < 4294967293) */
 {4294967295, 0}	/* __F	highest 32 bits number	(0 ite, 3*3 < 4294967295) */
};

int main(void)
{
	unsigned short i, r;				/* loop index and test result */
	unsigned short num_ok = 0;			/* number of right tests */
	unsigned short num_tests = 			/* total number of tests */
					sizeof(test_case) / sizeof(test_struct);
	
	printf("Testing the is_prime() function\n");
	/********* evaluate the list of test case values *********/
	for (i = 0; i < num_tests; i++)
	{
		r = is_prime(test_case[i].num);
		printf("\tcase %2d:\tnum= %10u\tis prime? %d\ttest ok? %d\n",
						i, test_case[i].num, test_case[i].xprime,
						(test_case[i].xprime == r));
		if (test_case[i].xprime == r)
			num_ok++;
	}
	printf("\nTotal number of tests = %d\n", num_tests);
	printf("Number of right tests = %d\n", num_ok);

	return(0);
}
