/*-----------------------------------------------------------------------
|   Description: a function to check whether a positive number is prime
| -----------------------------------------------------------------------*/

/* is_prime(n): returns 1 if the input number (n) is prime, 0 otherwise	*/
/*		Constraints: n must be a number between 2 and 4294967295;		*/
/*					 if n < 2, it returns 0 (0 and 1 are not primes)	*/
int is_prime(unsigned int n)
{
	int result = 0;							/* default result value */
	unsigned int div;						/* testing divisor */
	
	if (n >= 2)								/* avoid special cases 0 and 1 */
	{
		if ((n == 2) || (n == 3))			/* also avoid cases 2 and 3 */
			result = 1;						/* (don't fit in next algorithm) */
		else
		if (n % 2 != 0)						/* avoid even numbers (not prime) */
		{
			div = 3;						/* initial testing divisor */
			while ((n % div != 0)			/* loop until exact division or */
					&& (div <= n/div))		/* divisor > sqrt(n) */
				div += 2;					/* go for next odd divisor */
			
			result = (div > n/div);			/* check if loop ended because div*/
		}							   /* exceeded the limit (n not divisible)*/
	}
	return(result);
}
