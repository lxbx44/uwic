/*-----------------------------------------------------------------------
|   Prototype of a function for checking if a positive number is prime
| -----------------------------------------------------------------------*/

extern int is_prime(unsigned int n);
