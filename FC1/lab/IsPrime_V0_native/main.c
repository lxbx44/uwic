/*---------------------------------------------------------------------------
|   Description: a program to check whether a positive number is prime
|	Version V0 (native MINGW32): ask numbers to the user and say if every
|				number is prime or not, until he/she enters a 0
|	IMPORTANT NOTE: the function is_prime() is bugged on purpose, since it is
|				intended as an example of a debugging process!
|----------------------------------------------------------------------------
|	Author: Santiago Romani (DEIM, URV)
|	Date:   May/2020
| ---------------------------------------------------------------------------*/

#include <stdio.h>		/* extern declarations of libc functions: scanf(),
																  printf() */

/* is_prime(n): returns 1 if the input number (n) is prime, 0 otherwise	*/
/*		Constraints: n should be a number between 2 and 4294967295;		*/
/*					 if n < 2, it returns 0 (0 and 1 are not primes)	*/
int is_prime(unsigned int n)
{
	int result = 0;							/* default result value */
	unsigned int div;						/* testing divisor */
	
	if (n >= 2)								/* avoid special cases 0 and 1 */
	{
		if ((n == 2) || (n == 3))			/* also avoid cases 2 and 3 */
			result = 1;						/* (don't fit in next algorithm) */
		else
		if (n % 2 != 0)						/* avoid even numbers (not prime) */
		{
			div = 3;						/* initial testing divisor */
			while ((n % div != 0)			/* loop until exact division or */
					&& (div*div < n))		/* divisor >= sqrt(n) */
				div += 2;					/* go for next odd divisor */
			
			result = (div*div > n);			/* check if loop ended because div*/
		}							   /* exceeded the limit (n not divisible)*/
	}
	return(result);
}


int main(void)
{
	unsigned int num;

	printf("Please, type natural numbers between 2 and 4294967295 (0 to finish):\n");
	do		/* main testing loop: ask a number to the user and check whether */
	{		/* it is a prime number or not, until the user inputs a zero */
		scanf("%u", &num);
		printf("\t%u is", num);
		if (!is_prime(num))
			printf(" not");
		printf(" prime\n\n");
	} while (num != 0);

	return(0);
}
