@;=== startup routine for calling the main function of programs intended ===
@;=== for running on ARM-EABI platform ===

.text
		.align 2
		.arm
		.global _start
	_start:
		nop             @; dummy instr. to allow to set the initial breakpoint
		bl main		    @; before calling the main function
	.Lstop:
		b  .Lstop       @; endless loop

.end
