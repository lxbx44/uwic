/*----------------------------------------------------------------
|   Testing divmod routine; santiago.romani@urv.cat (May 2020)
| ----------------------------------------------------------------*/

#include "divmod.h"
#include "random.h"


/* type definition of the structured record that holds the test case values */
typedef struct {
	unsigned int num;		/* num and den are the input parameters of div_mod() */
	unsigned int den;
	unsigned int xquo;		/* xquo and xmod are the expected output values of */
	unsigned int xmod;		/* div_mod(num, den, &quo, &mod) */
} test_struct;

/* the list of test case values */
test_struct test_case[] =
{{0, 0, 0, 0},							/*  0: division by zero */
 {0, 1, 0, 0},							/*  1: num == 0, den != 0 */
 {1, 1, 1, 0},							/*  2: num == den (lowest values) */
 {2, 1, 2, 0},							/*  3: num > den (lowest values) */
 {1, 2, 0, 1},							/*  4: num < den (lowest values) */
 {2, 3, 0, 2},							/*  5: num < den, mod > 1 (lowest values) */
 {3, 3, 1, 0},							/*  6: division by 3, num == den */
 {4, 2, 2, 0},							/*  7: division by 2, mod == 0 */
 {40, 1, 40, 0},						/*  8: division by 1, num slightly high */
 {40, 3, 13, 1}, 						/*  9: division by 3, num slightly high, mod != 0 */
 {5, 4294967295, 0, 5},					/* 10: maximum den, num < den */
 {4294967295, 5, 858993459, 0},			/* 11: maximum num, division by 5, mod == 0 */
 {4294967295, 4294967295, 1, 0},		/* 12: maximum num and den */
 {2147483648, 13524818, 158, 10562404},	/* 13: num = 2^31, huge mod */
 {3189745, 2789, 1143, 1918}, 			/* 14: arbitrary numbers, relatively high mod */
 {328329,  573, 573, 0} 				/* 15: square number divided by square, quo == mod */
};


int main(void)
{
	unsigned int i;						/* loop index */
	unsigned int rnum, rden;			/* random input numbers */
	unsigned int tquo, tmod, terr;		/* routine results */
	unsigned int num_ok = 0;			/* number of right tests */
	unsigned int num_tests = 			/* total number of tests */
					sizeof(test_case) / sizeof(test_struct);

	/********* evaluate division by error *********/
	terr = div_mod(test_case[0].num, test_case[0].den, &tquo, &tmod);
	if (terr != 0) num_ok++;
	
	/********* evaluate the list of test case values *********/
	for (i = 1; i < num_tests; i++)
	{
		terr = div_mod(test_case[i].num, test_case[i].den, &tquo, &tmod);
		if ((tquo == test_case[i].xquo) && (tmod == test_case[i].xmod))
			num_ok++;
	}

/* TESTING POINT: check if number of ok tests (num_ok) is equal to number of
				 total tests (num_tests)
	(gdb) disp num_ok
	(gdb) disp num_tests
*/
	randomize();

	/********* evaluate random number divisions *********/
	for (i = 0; i < 1000000; i++)
	{
		rnum = random(0);
		rden = random(0);
		terr = div_mod(rnum, rden, &tquo, &tmod);
		if (terr && (rden == 0))
/* BREAKPOINT: check if there is a random division by 0 */
			num_ok++;
		else if ((tquo == rnum / rden) && (tmod == rnum % rden))
				num_ok++;
		num_tests++;
	}

/* BREAKPOINT */

	return(0);
}
