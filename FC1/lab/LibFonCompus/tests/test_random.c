/*----------------------------------------------------------------
|   Testing random routines; santiago.romani@urv.cat (May 2020)
| ----------------------------------------------------------------*/

#include "random.h"
#include "vsort.h"
#include <math.h>			/* need sqrt() function (also use libmath -lm) */

#define MAX_NUM 16			/* maximum number of elements in the vectors */

unsigned char vub[MAX_NUM];				/* list of unsigned bytes */
signed char vsb[MAX_NUM];				/* list of signed bytes */
unsigned short vuh[MAX_NUM];			/* list of unsigned halfwords */
signed short vsh[MAX_NUM];				/* list of signed halfwords */
unsigned int vuw[MAX_NUM];				/* list of unsigned words */
signed int vsw[MAX_NUM];				/* list of signed words */


void compute_statistics(unsigned short list[], unsigned short num_elems,
							float *m, float *std)
{
	unsigned short i;
	float mean, stdev;
	
	mean = 0;							/* compute mean */
	for (i = 0; i < num_elems; i++)
		mean += list[i];
	mean /= num_elems;
	
	stdev = 0;							/* compute standard deviation */
	for (i = 0; i < num_elems; i++)
		stdev += (mean - list[i]) * (mean - list[i]);
	stdev = sqrt(stdev) / (num_elems - 1);
	
	*m = mean;							/* pass results by reference */
	*std = stdev;
}


int main(void)
{
	unsigned short i, j;				/* loop indexes */
	unsigned short k;					/* histogram bin index */
	float mean, stdev, dfactor;			/* statistics */
	
	randomize();
	
	/********* fill in the six global vectors (and sort them) *********/
	for (i = 0; i < MAX_NUM; i++)
		vub[i] = mod_random(256);
	vsort_ub(vub, MAX_NUM);
	
	for (i = 0; i < MAX_NUM; i++)
		vsb[i] = 127 - mod_random(256);
	vsort_sb(vsb, MAX_NUM);

	for (i = 0; i < MAX_NUM; i++)
		vuh[i] = mod_random(65536);
	vsort_uh(vuh, MAX_NUM);
	
	for (i = 0; i < MAX_NUM; i++)
		vsh[i] = 32767 - mod_random(65536);
	vsort_sh(vsh, MAX_NUM);

	for (i = 0; i < MAX_NUM; i++)
		vuw[i] = random(0);
	vsort_uw(vuw, MAX_NUM);
	
	for (i = 0; i < MAX_NUM; i++)
		vsw[i] = random(1);
	vsort_sw(vsw, MAX_NUM);

/* BREAKPOINT: visually check if vectors contain evenly distributed values:
	(gdb) p /d vub
	(gdb) p /d vsb
	(gdb) p vuh
	(gdb) p vsh
	(gdb) p vuw
	(gdb) p vsw
*/
	i = 0;				/* dummy instruction for allowing a breakpoint stop */

	/********* make histograms for evaluating distribution *********/
	
/* TESTING POINT: check if normalized deviation of values (dfactor) is
				 relaitvely low (< 0.05)
	(gdb) disp mean
	(gdb) disp dfactor
*/
	/********* evaluate byte random numbers *********/
	for (i = 0; i < MAX_NUM; i++)					
		vuh[i] = 0;						/* reset counters */
	
	for (j = 0; j < 100; j++)			/* main loop */
	{
		for (i = 0; i < 1000; i++)		/* random generation loop */
		{
			k = mod_random(256) / (256 / MAX_NUM);
			vuh[k]++;					/* increase histogram bin */
		}
		compute_statistics(vuh, MAX_NUM, &mean, &stdev);
		dfactor = stdev / mean;
	}
/* BREAKPOINT */

	/********* evaluate halfword random numbers *********/
	for (i = 0; i < MAX_NUM; i++)					
		vuh[i] = 0;						/* reset counters */
	
	for (j = 0; j < 100; j++)			/* main loop */
	{
		for (i = 0; i < 1000; i++)		/* random generation loop */
		{
			k = mod_random(65536) / (65536 / MAX_NUM);
			vuh[k]++;					/* increase histogram bin */
		}
		compute_statistics(vuh, MAX_NUM, &mean, &stdev);
		dfactor = stdev / mean;
	}
/* BREAKPOINT */

	/********* evaluate unsigned word random numbers *********/
	for (i = 0; i < MAX_NUM; i++)					
		vuh[i] = 0;						/* reset counters */
	
	for (j = 0; j < 100; j++)			/* main loop */
	{
		for (i = 0; i < 1000; i++)		/* random generation loop */
		{
			k = random(0) / ((1 << 20) / MAX_NUM);
			vuh[k]++;					/* increase histogram bin */
		}
		compute_statistics(vuh, MAX_NUM, &mean, &stdev);
		dfactor = stdev / mean;
	}
/* BREAKPOINT */

	/********* evaluate signed word random numbers *********/
	for (i = 0; i < MAX_NUM; i++)					
		vuh[i] = 0;						/* reset counters */
	
	for (j = 0; j < 100; j++)			/* main loop */
	{
		for (i = 0; i < 1000; i++)		/* random generation loop */
		{
			k = (random(1) / ((1 << 20) / MAX_NUM)) + MAX_NUM / 2;
			if ((k >= 0) && (k < MAX_NUM))
				vuh[k]++;				/* increase histogram bin */
		}
		compute_statistics(vuh, MAX_NUM, &mean, &stdev);
		dfactor = stdev / mean;
	}
/* BREAKPOINT */
	dfactor = dfactor;				/* dummy instruction for avoiding warning */

	return(0);
}
