/*----------------------------------------------------------------
|   Testing vsort routines; santiago.romani@urv.cat (May 2020)
| ----------------------------------------------------------------*/

#include "vsort.h"
#include "random.h"

	/* unsigned bytes: one position */
unsigned char vub1[1] = {10};
	/* unsigned bytes: two positions, unsorted */
unsigned char vub2[2] = {20, 15};
	/* unsigned bytes: three positons, minimum in the middle, maximum first */
unsigned char vub3[3] = {30, 10, 20};
	/* signed bytes: four positions, with negative numbers */
signed char vsb1[4] = {10, -20, 0, -10};
	/* signed bytes: all negative numbers with repetitions */
signed char vsb2[5] = {-1, -1, -128, -127, -1};
	/* signed bytes: already sorted with repetions and full range values */
signed char vsb3[6] = {-128, -128, -1, 1, 127, 127};
	/* unsigned halfwords: all values below 256 */
unsigned short vuh1[7] = {0, 30, 12, 255, 9, 31, 29};
	/* unsigned halfwords: first position maximum, last position minimum */
unsigned short vuh2[8] = {32768, 1450, 8918, 15000, 200, 3276, 0, 0};
	/* unsigned halfwords: last value has the maximum range */
unsigned short vuh3[9] = {32767, 65535, 14500, 0, 1, 9000, 4, 4, 65535};
	/* signed halfwords: all negative range */
signed short vsh1[10] = {-32767, -32766, -32768, -3, -2, -1000, -1, -4, -4, -4};
	/* signed halfwords: combining all range */
signed short vsh2[11] = {-32767, 32767, -32768, -3, -1000, 1, -4, 4, -4, 4, -4};
	/* signed halfwords: already sorted */
signed short vsh3[12] = {-32768, -32767, -1000, -5, -4, -4, -3, 1, 4, 5, 32767,
						32767};
	/* unsigned words: single value */
unsigned int vuw1[13] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	/* unsigned words: maximum range */
unsigned int vuw2[14] = {0, 4294967295, 0, 4294967295, 0, 4294967295, 0,
						4294967295, 0, 4294967295, 4294967295, 0, 4294967294,
						0};
	/* unsigned words: inversed vector */
unsigned int vuw3[15] = {14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
	/* unsigned words: maximum range */
signed int vsw1[16] = {0, 2147483647, -2147483647, -2147483648, 0, -2147483647,
					  -1, 0, -1, -2, 1, 3, 2147483647, -2147483648, 0, 0};
	/* unsigned words: all values equal except one */
signed int vsw2[17] = {0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
					  -1, -1, -1};
	/* unsigned words: random values */
signed int vsw3[18] = {-4454, 3742, 4559, -3413, 2820, 865, 4838, -3983, -4290,
					  3524, 1842, 3906, -2647, 3300, -637, 1832, 3407, 673};


/********* auxiliar functions to check if a vector is rightly sorted *********/
unsigned char check_ub(unsigned char v[], unsigned short n)
{
	unsigned short i = 1;
	
	while ((i < n) && (v[i-1] <= v[i])) i++;
	return(i == n ? 1 : 0);
}

unsigned char check_sb(signed char v[], unsigned short n)
{
	unsigned short i = 1;
	
	while ((i < n) && (v[i-1] <= v[i])) i++;
	return(i == n ? 1 : 0);
}

unsigned char check_uh(unsigned short v[], unsigned short n)
{
	unsigned short i = 1;
	
	while ((i < n) && (v[i-1] <= v[i])) i++;
	return(i == n ? 1 : 0);
}

unsigned char check_sh(signed short v[], unsigned short n)
{
	unsigned short i = 1;
	
	while ((i < n) && (v[i-1] <= v[i])) i++;
	return(i == n ? 1 : 0);
}

unsigned char check_uw(unsigned int v[], unsigned short n)
{
	unsigned short i = 1;
	
	while ((i < n) && (v[i-1] <= v[i])) i++;
	return(i == n ? 1 : 0);
}

unsigned char check_sw(signed int v[], unsigned short n)
{
	unsigned short i = 1;
	
	while ((i < n) && (v[i-1] <= v[i])) i++;
	return(i == n ? 1 : 0);
}


int main(void)
{
	unsigned short i, j, k;				/* partial result */
	unsigned short num_ok = 0;			/* number of right tests */
	unsigned short num_tests = 0;		/* total number of tests */
	
	unsigned char *va_ub[] = {vub1, vub2, vub3};
	signed char *va_sb[] = {vsb1, vsb2, vsb3};
	unsigned short *va_uh[] = {vuh1, vuh2, vuh3};
	signed short *va_sh[] = {vsh1, vsh2, vsh3};
	unsigned int *va_uw[] = {vuw1, vuw2, vuw3};
	signed int *va_sw[] = {vsw1, vsw2, vsw3};
	
	/********* evaluate sorting routines on specific valued vectors *********/
	j = 1;
	/********* evaluate unsigned byte numbers *********/
	for (i = 0; i < 3; i++)
	{
		vsort_ub(va_ub[i], j);
		k = check_ub(va_ub[i], j++);
		num_ok += k;
		num_tests++;
	}
	
	/********* evaluate signed byte numbers *********/
	for (i = 0; i < 3; i++)
	{
		vsort_sb(va_sb[i], j);
		k = check_sb(va_sb[i], j++);
		num_ok += k;
		num_tests++;
	}
	
	/********* evaluate unsigned halfword numbers *********/
	for (i = 0; i < 3; i++)
	{
		vsort_uh(va_uh[i], j);
		k = check_uh(va_uh[i], j++);
		num_ok += k;
		num_tests++;
	}
	
	/********* evaluate signed halfword numbers *********/
	for (i = 0; i < 3; i++)
	{
		vsort_sh(va_sh[i], j);
		k = check_sh(va_sh[i], j++);
		num_ok += k;
		num_tests++;
	}
	
	/********* evaluate unsigned word numbers *********/
	for (i = 0; i < 3; i++)
	{
		vsort_uw(va_uw[i], j);
		k = check_uw(va_uw[i], j++);
		num_ok += k;
		num_tests++;
	}
	
	/********* evaluate signed word numbers *********/
	for (i = 0; i < 3; i++)
	{
		vsort_sw(va_sw[i], j);
		k = check_sw(va_sw[i], j++);
		num_ok += k;
		num_tests++;
	}
/* BREAKPOINT: visually check if vectors are properly sorted:
	(gdb) p /d vub1
	(gdb) p /d vub2
	(gdb) p /d vub3
	(gdb) p /d vsb1
	(gdb) p /d vsb2
	(gdb) ...
*/
	i = 0;				/* dummy instruction for allowing a breakpoint stop */
	
	/********* evaluate sorting routines on randomly valued vectors *********/

/* TESTING POINT: check if number of right tests (num_ok) is equal to number of
				 total tests (num_tests)
	(gdb) disp num_ok
	(gdb) disp num_tests
*/
	randomize();
	/********* evaluate unsigned byte numbers *********/
	for (j = 0; j < 1000; j++)
	{
		for (i = 0; i < 3; i++)
			vub3[i] = mod_random(256);
		vsort_ub(vub3, 3);
		k = check_ub(vub3, 3);
		num_ok += k;
		num_tests++;
	}

	/********* evaluate signed byte numbers *********/
	for (j = 0; j < 1000; j++)
	{
		for (i = 0; i < 6; i++)
			vsb3[i] = 127 - mod_random(256);
		vsort_sb(vsb3, 6);
		k = check_sb(vsb3, 6);
		num_ok += k;
		num_tests++;
	}

	/********* evaluate unsigned halfword numbers *********/
	for (j = 0; j < 1000; j++)
	{
		for (i = 0; i < 9; i++)
			vuh3[i] = mod_random(65536);
		vsort_uh(vuh3, 9);
		k = check_uh(vuh3, 9);
		num_ok += k;
		num_tests++;
	}

	/********* evaluate signed halfword numbers *********/
	for (j = 0; j < 1000; j++)
	{
		for (i = 0; i < 12; i++)
			vsh3[i] = 32767 - mod_random(65536);
		vsort_sh(vsh3, 12);
		k = check_sh(vsh3, 12);
		num_ok += k;
		num_tests++;
	}

	/********* evaluate unsigned word numbers *********/
	for (j = 0; j < 1000; j++)
	{
		for (i = 0; i < 15; i++)
			vuw3[i] = random(0);
		vsort_uw(vuw3, 15);
		k = check_uw(vuw3, 15);
		num_ok += k;
		num_tests++;
	}

	/********* evaluate signed word numbers *********/
	for (j = 0; j < 1000; j++)
	{
		for (i = 0; i < 18; i++)
			vsw3[i] = random(1);
		vsort_sw(vsw3, 18);
		k = check_sw(vsw3, 18);
		num_ok += k;
		num_tests++;
	}
	
/* BREAKPOINT */

	return(0);
}
