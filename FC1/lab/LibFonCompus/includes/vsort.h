/*----------------------------------------------------------------
|   Prototypes of routines for sorting vectors of basic types
|				(signed and unsigned bytes, halwords and words)
|				in ascending order
| ----------------------------------------------------------------*/

#ifndef VSORT_H
#define VSORT_H


/* vsort_ub():	sorts a vector of unsigned bytes (8 bits)
	Parameters:
		(v[])	initial address of the vector
		(n)		number of elements of the vector
	Result:	vector elements get sorted in ascending order
*/
extern void vsort_ub(unsigned char v[], unsigned short n);

/* vsort_ub():	sorts a vector of signed bytes (8 bits) */
extern void vsort_sb(signed char v[], unsigned short n);


/* vsort_uh():	sorts a vector of unsigned halfwords (16 bits) */
extern void vsort_uh(unsigned short v[], unsigned short n);

/* vsort_sh():	sorts a vector of signed halfwords (16 bits) */
extern void vsort_sh(signed short v[], unsigned short n);


/* vsort_uh():	sorts a vector of unsigned words (32 bits) */
extern void vsort_uw(unsigned int v[], unsigned short n);

/* vsort_uh():	sorts a vector of signed words (32 bits) */
extern void vsort_sw(signed int v[], unsigned short n);


#endif /* VSORT_H */
