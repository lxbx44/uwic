/*----------------------------------------------------------------
|   Prototypes of routines for generating pseudo-random numbers
| ----------------------------------------------------------------*/

#ifndef RANDOM_H
#define RANDOM_H


/* randomize():	initializes the seed number to a random value, to avoid
				repetition of the pseudo-random sequence
*/
extern void randomize();



/* random():	provides the next pseudo-random number of the sequence,
				based on the previous seed value.
	Parameters:
		(integer)	if 0, generates a natural value,
					if not 0, generates an integer value 
	Result:		the next pseudo-random number of the sequence;
				output ranges are around [0..2^21] | [-2^20..2^20]
*/
extern int random(unsigned char integer);



/* mod_random(): returns a pseudo-random number within a constrained range
	Parameters:
		(n)		width of the range; it must be between 2 and 65536 (2^16)
					otherwise, the routine will trim the input value into
					these limits
	Result:		the pseudo-random number within the range [0..n-1]
*/
extern unsigned short mod_random(unsigned int n);


#endif /* RANDOM_H */
