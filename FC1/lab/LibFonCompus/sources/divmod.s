@;== divmod.s: routine for computing integer division  ==
@;== santiago.romani@urv.cat (May 2020) ==

.text
		.align 2
		.arm

@; int div_mod(unsigned int num, unsigned int den,
@;				unsigned int *quo, unsigned int *mod):
@;				computes the natural division (num / den) and returns
@;				quotient and remainder (by reference); it also returns
@;				an error code, 0 -> everything OK, 1 -> division by 0
@;	Parameters:
@;		R0 -> (num) 	numerator
@;		R1 -> (den)		denominator
@;		R2 -> (* quo)	address of the variable that will hold the quotient,
@;		R3 -> (* mod)	address of the variable that will hold the remainder
@;	Results:
@; 		R0 <- returns 0 if no problem, !=0 if there is a division error (den==0)
		.global div_mod
div_mod:
		push {r4-r7, lr}
		
		cmp r1, #0					@; check division by zero
		moveq r0, #1				@; return error code
		beq .Ldiv_end				@; skip the rest of the routine
		
		mov r4, #0					@; R4 holds the quotient (q)
		mov r5, #0					@; R5 holds the remainder (r)
		cmp r0, #0					@; check if numerator is zero
		beq .Ldiv_numer0			@; special case (numerator = 0 -> q=0 y r=0)
		
		clz r6, r0					@; count leading zeros on numerator
		mov r7, #0x80000000			@; R7 is a bitmask
		mov r7, r7, lsr r6			@; set bitmask on highest bit 1 of numerator
	.Ldiv_for:
		tst r7, r0					@; if i-th bit of numerator == 1,
		orrne r5, #1				@; 	r = r | 1;
		cmp r5, r1					@; if r >= denominator, 
		subhs r5, r1				@; 	r = r - denominator;
		orrhs r4, #1				@; 	q = q | 1;
		mov r7, r7, lsr #1			@; shift bitmask to next lower-weight bit
		cmp r7, #0					@; if bit mask != 0,
		movne r5, r5, lsl #1		@;	shift remainder and quotient
		movne r4, r4, lsl #1		@;
		bne .Ldiv_for				@; repeat until bit mask == 0
	.Ldiv_numer0:
		str r4, [r2]
		str r5, [r3]				@; store results in memory (by reference)
		mov r0, #0					@; return code OK
	.Ldiv_end:
		
		pop {r4-r7, pc}


.end
