@;== vsort.s : 	routines for sorting vectors of different basic types		==
@;==			(signed and unsigned bytes, halwords and words)				==
@;== NOTE :	although the code of these six routines is highly repetitive, 	==
@;			it is left as is intentionally in order to boost execution		==
@;			performance, otherwise the software organization for reusing 	==
@;			the code as much as possible will be rather intrincate and		==
@;			ineficient; the best way would be to rely on abstract data		==
@;			types, where the elements to be sorted provide getters,	setters ==
@;			and a comparison function, but this approach is out	of the		==
@;			scope of the academic level for which this software is intended ==
@;== santiago.romani@urv.cat (May 2020) ==

.text
		.arm
		.align 2


@; void vsort_ub(unsigned char v[], unsigned short n): sorts a vector of
@;				unsigned bytes (8 bits)
@;	Parameters:
@;		R0 -> (v[])	initial address of the vector
@;		R1 -> (n)	number of elements of the vector
@;	Result:	elements of the vector get sorted in ascending order
	.global vsort_ub
vsort_ub:
		push {r0-r7, lr}
		
		cmp r1, #1				@; avoid lenght < 1
		ble .Lub_end
		add r1, r0, r1 			@; R1 is address after vector
		sub r2, r1, #1			@; R2 is address of last vector position
	.Lub_fori:
		mov r3, r0				@; R3 is address of v[index] (initially @v[i])
		ldrb r4, [r3]			@; R4 is min (initially v[i])
		add r5, r3, #1			@; R5 is address of v[j] (initially @v[i+1])
	.Lub_forj:
		ldrb r6, [r5]			@; R6 = v[j]
		cmp r6, r4				@; check whether v[j] < min
		bhs .Lub_notmin			@; jump at end of forj loop otherwise
		mov r3, r5				@; R3 keeps address of v[j] (@v[index])
		mov r4, r6				@; min = v[index]
	.Lub_notmin:
		add r5, #1
		cmp r5, r1				@; close secondary loop
		blo .Lub_forj
		cmp r3, r0				@; check whether index != i
		beq .Lub_equals			@; skip swapping otherwise
		ldrb r7, [r0]			@; R7 = v[i]
		strb r4, [r0]			@; v[i] = min
		strb r7, [r3]			@; v[index] = R7
	.Lub_equals:
		add r0, #1
		cmp r0, r2				@; close primary loop
		blo .Lub_fori
	.Lub_end:		
		pop {r0-r7, pc}


@; void vsort_sb(signed char v[], unsigned short n): sorts a vector of
@;				signed bytes (8 bits)
@;	Parameters:
@;		R0 -> (v[])	initial address of the vector
@;		R1 -> (n)	number of elements of the vector
@;	Result:	elements of the vector get sorted in ascending order
	.global vsort_sb
vsort_sb:
		push {r0-r7, lr}
		
		cmp r1, #1				@; avoid lenght < 1
		ble .Lsb_end
		add r1, r0, r1 			@; R1 is address after vector
		sub r2, r1, #1			@; R2 is address of last vector position
	.Lsb_fori:
		mov r3, r0				@; R3 is address of v[index] (initially @v[i])
		ldsb r4, [r3]			@; R4 is min (initially v[i])
		add r5, r3, #1			@; R5 is address of v[j] (initially @v[i+1])
	.Lsb_forj:
		ldsb r6, [r5]			@; R6 = v[j]
		cmp r6, r4				@; check whether v[j] < min
		bge .Lsb_notmin			@; jump at end of forj loop otherwise
		mov r3, r5				@; R3 keeps address of v[j] (@v[index])
		mov r4, r6				@; min = v[index]
	.Lsb_notmin:
		add r5, #1
		cmp r5, r1				@; close secondary loop
		blo .Lsb_forj
		cmp r3, r0				@; check whether index != i
		beq .Lsb_equals			@; skip swapping otherwise
		ldsb r7, [r0]			@; R7 = v[i]
		strb r4, [r0]			@; v[i] = min
		strb r7, [r3]			@; v[index] = R7
	.Lsb_equals:
		add r0, #1
		cmp r0, r2				@; close primary loop
		blo .Lsb_fori
	.Lsb_end:		
		pop {r0-r7, pc}



@; void vsort_uh(unsigned short v[], unsigned short n): sorts a vector of
@;				unsigned halfwords (16 bits)
@;	Parameters:
@;		R0 -> (v[])	initial address of the vector
@;		R1 -> (n)	number of elements of the vector
@;	Result:	elements of the vector get sorted in ascending order
	.global vsort_uh
vsort_uh:
		push {r0-r7, lr}
		
		cmp r1, #1				@; avoid lenght < 1
		ble .Luh_end
		add r1, r0, r1, lsl #1	@; R1 is address after vector
		sub r2, r1, #2			@; R2 is address of last vector position
	.Luh_fori:
		mov r3, r0				@; R3 is address of v[index] (initially @v[i])
		ldrh r4, [r3]			@; R4 is min (initially v[i])
		add r5, r3, #2			@; R5 is address of v[j] (initially @v[i+1])
	.Luh_forj:
		ldrh r6, [r5]			@; R6 = v[j]
		cmp r6, r4				@; check whether v[j] < min
		bhs .Luh_notmin			@; jump at end of forj loop otherwise
		mov r3, r5				@; R3 keeps address of v[j] (@v[index])
		mov r4, r6				@; min = v[index]
	.Luh_notmin:
		add r5, #2
		cmp r5, r1				@; close secondary loop
		blo .Luh_forj
		cmp r3, r0				@; check whether index != i
		beq .Luh_equals			@; skip swapping otherwise
		ldrh r7, [r0]			@; R7 = v[i]
		strh r4, [r0]			@; v[i] = min
		strh r7, [r3]			@; v[index] = R7
	.Luh_equals:
		add r0, #2
		cmp r0, r2				@; close primary loop
		blo .Luh_fori
	.Luh_end:		
		pop {r0-r7, pc}


@; void vsort_sh(signed short v[], unsigned short n): sorts a vector of
@;				signed halfwords (16 bits)
@;	Parameters:
@;		R0 -> (v[])	initial address of the vector
@;		R1 -> (n)	number of elements of the vector
@;	Result:	elements of the vector get sorted in ascending order
	.global vsort_sh
vsort_sh:
		push {r0-r7, lr}
		
		cmp r1, #1				@; avoid lenght < 1
		ble .Lsh_end
		add r1, r0, r1, lsl #1	@; R1 is address after vector
		sub r2, r1, #2			@; R2 is address of last vector position
	.Lsh_fori:
		mov r3, r0				@; R3 is address of v[index] (initially @v[i])
		ldsh r4, [r3]			@; R4 is min (initially v[i])
		add r5, r3, #2			@; R5 is address of v[j] (initially @v[i+1])
	.Lsh_forj:
		ldsh r6, [r5]			@; R6 = v[j]
		cmp r6, r4				@; check whether v[j] < min
		bge .Lsh_notmin			@; jump at end of forj loop otherwise
		mov r3, r5				@; R3 keeps address of v[j] (@v[index])
		mov r4, r6				@; min = v[index]
	.Lsh_notmin:
		add r5, #2
		cmp r5, r1				@; close secondary loop
		blo .Lsh_forj
		cmp r3, r0				@; check whether index != i
		beq .Lsh_equals			@; skip swapping otherwise
		ldsh r7, [r0]			@; R7 = v[i]
		strh r4, [r0]			@; v[i] = min
		strh r7, [r3]			@; v[index] = R7
	.Lsh_equals:
		add r0, #2
		cmp r0, r2				@; close primary loop
		blo .Lsh_fori
	.Lsh_end:		
		pop {r0-r7, pc}



@; void vsort_uw(unsigned int v[], unsigned short n): sorts a vector of
@;			unsigned words (32 bits)
@;	Parameters:
@;		R0 -> (v[])	initial address of the vector
@;		R1 -> (n)	number of elements of the vector
@;	Result:	elements of the vector get sorted in ascending order
	.global vsort_uw
vsort_uw:
		push {r0-r7, lr}
		
		cmp r1, #1				@; avoid lenght < 1
		ble .Luw_end
		add r1, r0, r1, lsl #2	@; R1 is address after vector
		sub r2, r1, #4			@; R2 is address of last vector position
	.Luw_fori:
		mov r3, r0				@; R3 is address of v[index] (initially @v[i])
		ldr r4, [r3]			@; R4 is min (initially v[i])
		add r5, r3, #4			@; R5 is address of v[j] (initially @v[i+1])
	.Luw_forj:
		ldr r6, [r5]			@; R6 = v[j]
		cmp r6, r4				@; check whether v[j] < min
		bhs .Luw_notmin			@; jump at end of forj loop otherwise
		mov r3, r5				@; R3 keeps address of v[j] (@v[index])
		mov r4, r6				@; min = v[index]
	.Luw_notmin:
		add r5, #4
		cmp r5, r1				@; close secondary loop
		blo .Luw_forj
		cmp r3, r0				@; check whether index != i
		beq .Luw_equals			@; skip swapping otherwise
		ldr r7, [r0]			@; R7 = v[i]
		str r4, [r0]			@; v[i] = min
		str r7, [r3]			@; v[index] = R7
	.Luw_equals:
		add r0, #4
		cmp r0, r2				@; close primary loop
		blo .Luw_fori
	.Luw_end:		
		pop {r0-r7, pc}


@; void vsort_sh(signed int v[], unsigned short n): sorts a vector of
@;			signed words (32 bits)
@;	Parameters:
@;		R0 -> (v[])	initial address of the vector
@;		R1 -> (n)	number of elements of the vector
@;	Result:	elements of the vector get sorted in ascending order
	.global vsort_sw
vsort_sw:
		push {r0-r7, lr}
		
		cmp r1, #1				@; avoid lenght < 1
		ble .Lsw_end
		add r1, r0, r1, lsl #2	@; R1 is address after vector
		sub r2, r1, #4			@; R2 is address of last vector position
	.Lsw_fori:
		mov r3, r0				@; R3 is address of v[index] (initially @v[i])
		ldr r4, [r3]			@; R4 is min (initially v[i])
		add r5, r3, #4			@; R5 is address of v[j] (initially @v[i+1])
	.Lsw_forj:
		ldr r6, [r5]			@; R6 = v[j]
		cmp r6, r4				@; check whether v[j] < min
		bge .Lsw_notmin			@; jump at end of forj loop otherwise
		mov r3, r5				@; R3 keeps address of v[j] (@v[index])
		mov r4, r6				@; min = v[index]
	.Lsw_notmin:
		add r5, #4
		cmp r5, r1				@; close secondary loop
		blo .Lsw_forj
		cmp r3, r0				@; check whether index != i
		beq .Lsw_equals			@; skip swapping otherwise
		ldr r7, [r0]			@; R7 = v[i]
		str r4, [r0]			@; v[i] = min
		str r7, [r3]			@; v[index] = R7
	.Lsw_equals:
		add r0, #4
		cmp r0, r2				@; close primary loop
		blo .Lsw_fori
	.Lsw_end:		
		pop {r0-r7, pc}


.end
