@;== random.s: routines for generating pseudo-random numbers  ==
@;== santiago.romani@urv.cat (May 2020) ==

.data
		.align 2
		.global seed32			@; numeric seed for next pseudo-random number
	seed32:	.word	-1			@; (initial seed cannot be zero)

.text
		.align 2
		.arm


@; void randomize(): initializes seed32 to a random value, to avoid repetition
@;				of the pseudo-random sequence; it can be done by copying the
@;				value stored in absolute memory position 0x10004, which changes
@;				every time the program is loaded for execution (?)
		.global randomize
randomize:
		push {r0-r1, lr}
		
		ldr r0, =0x10004
		ldr r1, [r0]			@; get the initial random value
		cmp r1, #0
		moveq r1, #-1			@; avoid the zero value (set -1 instead)
		ldr r0, =seed32
		str r1, [r0]			@; update the seed32 global variable
		
		pop {r0-r1, pc}



@; int random(unsigned char integer): provides the next pseudo-random number
@;				of the sequence, based on the previous seed value (stored in
@;				the seed32 global variable)
@;	Parameters:
@;		R0 -> (integer)	indicates if the random value must be natural (==0)
@;						or integer (!=0)
@;	Result:
@;		R0 <-	next pseudo-random number of the sequence (seed32 gets updated);
@;				output ranges are around [0..2^21] | [-2^20..2^20]
		.global random
random:
		push {r1-r5, lr}
			
		ldr r1, =seed32
		ldr r2, [r1]			@; R2 =  current value of seed32
		ldr r3, =0x0019660D
		cmp r0, #0				@; checks whether integer value must be created
		smullne r4, r5, r2, r3
		umulleq r4, r5, r2, r3
		ldr r3, =0x3C6EF35F
		adds r4, r3
		adc r5, #0				@; R5:R4 = new pseudo-random value (64 bits)
		str r4, [r1]			@; low 32 bits are the new seed32
		mov r0, r5				@; high 32 bits are the new result
			
		pop {r1-r5, pc}	



@; unsigned short mod_random(unsigned int n): returns a pseudo-random number
@;				within a constrained range
@;	Parameters:
@;		R0 -> (n)	width of the range; it must be between 2 and 65536 (2^16)
@;					otherwise, the routine will trim the input value into
@;					these limits
@;	Result:
@;		R0 <- 		the pseudo-random number within the range [0..n-1]
	.global mod_random
mod_random:
		push {r2-r3, lr}
		
		cmp r0, #65536
		movhi r0, #65536		@; trim the maximum value for n
		cmp r0, #2
		movlo r0, #2			@; trim the minimum value for n
		sub r2, r0, #1			@; R2 = n-1 (the highest allowed value)
		cmp r0, #256
		movls r3, #1			@; R3 is a bit mask
		movhi r3, #255			@; speeds up bit mask calc. for high n values
	.Lmodran_formask:
		mov r3, r3, lsl #1
		orr r3, #1				@; enlarges the mask with a new bit (1)
		cmp r3, r2				@; repeats until the mask is greater than the
		blo .Lmodran_formask	@; maximum range value
		
	.Lmodran_loop:
		mov r0, #0
		bl random				@; gets a new natural pseudo-random of 32 bits
		and r0, r3				@; trim the lower bits according to the mask
		cmp r0, r2				@; if result is above the maximum range value,
		bhi .Lmodran_loop		@; repeat the process
		
		pop {r2-r3, pc}


.end
