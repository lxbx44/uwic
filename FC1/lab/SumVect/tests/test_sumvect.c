/*-----------------------------------------------------------------------
|   Description: a program to test the sumvect_sb() routine
|------------------------------------------------------------------------
|	Authors: Santiago Romani & Pere Millan (DEIM, URV)
|	Last rev.: March 2021
| -----------------------------------------------------------------------*/

#include "sumvect.h"		/* extern declaration of sumvect_sb() function */

/* sample vectors for different test cases */
signed char vsb0[] = {};
signed char vsb1[] = {30};
signed char vsb2[] = {-25, 14};	
signed char vsb3[] = {100, 38, 49};	
signed char vsb4[] = {30, 54, 127, 0, 126, 50, 28, 101};
signed char vsb5[] = {-30, -54, -128, -1, -127, -50, -28, -100,
					  -109, -99, -12, -1, -87, -105, -29, -65};
signed char vsb6[] = {-30, -54, -128, -1, -127, -50, -28, -100,
					  109, 99, 12, 1, 87, 105, 29, 65};
signed char vsb7[640];
signed char vsb8[640];


/* type definition of the structured record that holds the test case values */
typedef struct {
	signed char *vect;			// input vector (address)
	unsigned short num;			// number of vector elements
	int xsum;					// expected output result from sumvect_sb()
} test_struct;


/* the list of test case values; the letter labels (such as '_A_') at the
	beginning of each comment indicates a specific type of test; when one
	type appears for the first time in the list, the letter is in-between
	two underscores ('_A_'), otherwise, the letter is after two underscores
	('__A'); theoretically, one could remove the repeated cases and the test
	set would be either confident, although sometimes there exist unexpected
	bugs on apparently similar types of tests */
test_struct test_case[] =
{{vsb0, 0, 0},			// _A_	special case num = 0 	(result = 0)
 {vsb1, 1, 30},			// _B_	special case num = 1 	(result = vsb1[0])
 {vsb2, 2, -11},		// _C_	num > 1, positive & negative values
 {vsb3, 3, 187},		// _D_	all positives, result > 8 bits Ca2 limit +127
 {vsb4, 8, 516},		// __D	result > 8 bits nat limit 255
 {vsb5, 16, -1025},		// _E_	all negatives, result < 8 bits Ca2 limit -128
 {vsb6, 16, -11},		// _F_  several pos.&neg., compensating each other
 {vsb7, 640, 70080},	// _G_	long vector, result > 16 bits Ca2 limit +32767
 {vsb8, 640, -70080}	// _H_	long vector, result < 16 bits Ca2 limit -32768
};


/* Helper function to set up long vectors vsb7[] and vsb8[], with 640 positive
	or negative values, respectively, in order to create summations that cross
	the 16-bit border [-32768..32767].
*/
void init_vectors()
{
	unsigned short i;
	signed char var = 0;			// one varying offset
	
	for (i = 0; i < 640; i++)		// assuming test vectors have 640 bytes
	{
		vsb7[i] = (signed char) (100 + var);	// store numbers in [100..119]
		vsb8[i] = (signed char) (-100 -var);	// idem in [-119..-100]
		var = (var + 1) % 20;		// keep var in [0..19]
	}
}


int main(void)
{
	unsigned short i;					// loop index
	int r;								// result from tested function
	unsigned short num_ok = 0;			// number of right tests
	unsigned short num_tests = 			// total number of tests
					sizeof(test_case) / sizeof(test_struct);
	
	init_vectors();
	
	/********* evaluate the list of test case values *********/
	for (i = 0; i < num_tests; i++)
	{
		r = sumvect_sb(test_case[i].vect, test_case[i].num);
		if (test_case[i].xsum == r)
			num_ok++;
	}

/* TESTING POINT: check if number of ok tests (num_ok) is equal to number of
				 total tests (num_tests)
	(gdb) p num_ok
	(gdb) p num_tests
*/
	return(0);
}
