/*----------------------------------------------------------------------
|	Authors: Pere Millan & Santiago Romani (DEIM, URV)
|	Last rev.:  March 2021
|-----------------------------------------------------------------------|
|   Description: C-language headers of routines defined in sumvect.s
| ----------------------------------------------------------------------*/

#ifndef SUMVECT_H
#define SUMVECT_H

		/* routine to sum all values (signed bytes) within a vector */
extern int sumvect_sb(signed char v[], unsigned short n);


#endif

