@;===========================================================================
@;	Description: a sample program to sum all values within a vector
@;	Observations:
@;		* the main() routine is usually invoked from the _start() routine,
@;				   	defined in the startup.s module,
@;		* this main() simply calls the sum_vect() routine, passing one sample
@;					vector defined in .data section,
@;		* the return value of the sumvect_sb() is stored in a global variable
@;					defined in .bss section.
@;===========================================================================
@;	Authors: Santiago Romani & Pere Millan (DEIM, URV)
@;	Last rev.: March 2021
@;===========================================================================


@;--- .data : initialized data ---
.data
	vector:	.byte	1, -2, 30, 42, -5, 16, 77, 92, 0xFF, 0xA0
			.byte	12, 0b00110110, 4, 255, 4, -127


@;-- .bss : non-initialized data ---
.bss
		.align 2
	summ:	.space	4


@;-- .text : program code ---
.text
		.align 2
		.arm

@; main:	example of main routine, calling the sum_vect() routine once,
@;		 	using a sample vector defined above
		.global main
main:
		push {r0-r1, lr}	@; save values of working regs into the stack
			
			@; summ = sum_vect(vector, 16);
		ldr r0, =vector		@; R0 = base address of sample vector
		mov r1, #16			@; R1 = number of elements of sample vector
		bl sumvect_sb		@; call the routine; R0 will return the result
		ldr r1, =summ		@; R1 = address of global variable 'summ'
		str r0, [r1]		@; store the result into the variable (in memory)
			
		pop {r0-r1, pc}		@; restore values of working regs from the stack
							@;  and returns to the caller

.end
