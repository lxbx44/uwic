@;===========================================================================
@;	Description: a routine to sum all values within a vector
@;===========================================================================
@;	Authors: Santiago Romani & Pere Millan (DEIM, URV)
@;	Last rev.: March 2021
@;===========================================================================


@;-- .text : program code ---
.text
		.align 2
		.arm

@; int sumvect_sb(signed char *v, unsigned short n);
@; 	description: accumulates all elements (signed bytes) contained in a vector
@;	observations:
@;		* the vector must contain signed byte elements,
@;		* the lenght of the vector can be between 0 and 65535.
@; 	parameters:
@;		R0: base address of the vector (signed char *v)
@;		R1: number of vector elements (unsigned short n)
@;	result:
@;		R0: summation of all vector elements (int)
		.global sumvect_sb
sumvect_sb:
		push {r2-r4, lr}	@; save values of working regs into the stack
		
		mov r2, r0			@; R2 holds a copy of base address
		mov r0, #0			@; initialization of the result
		cmp r1, #0			@; if number of elements is zero,
		beq .Lsv_end		@;  skip the whole computation
		mov r3, #0			@; R3: index of vector elements
	.Lsv_do:				@; do-while structure
		ldsb r4, [r2, r3]	@; R4: value of v[index] (with Ca2 sign extension)
		add r0, r4			@; result += v[index];
		add r3, #1			@; index++;
		cmp r3, r1
		blo .Lsv_do			@; repeat while index < num. elems
	.Lsv_end:
		pop  {r2-r4, pc}	@; restore values of working regs from the stack
							@;  and returns to the caller (R0 holds the summ)

.end
