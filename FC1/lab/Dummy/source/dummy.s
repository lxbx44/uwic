@;=== ARM assembly for testing machine language instructions within GDB ===

.text
		.global _start
	_start:						@; _start label defines entry point for linker
	main:						@; main label defines entry point for gdb
		nop
		nop						@; 15 "empty" positions,
		nop						@; for injecting new machine lang. instructions
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
	.Liloop:
		nop
		b .Liloop				@; infinite loop

.end
