# Dia 2

- Mon Feb 19

## Operacions amb nombres binaris

|Rure| A +B  | Suma | Carry |
|----|-------|------|-------|
| 1  | 0 + 0 |  0   |   0   |
| 2  | 0 + 1 |  1   |   0   |
| 3  | 1 + 0 |  1   |   0   |
| 4  | 1 + 1 |  1   |   1   |


## Els nombres enters

- Per saber el signe, s'utilitza el primer bit
    | S | bit | bit | ... | bit | 
    |---|-----|-----|-----|-----|

- Complement a  2 (Ca2)
    És la codificació entera utilitzada perquè és la més fàcil d'operar

    - Apliquem Ca1

        $+5 \rightarrow 0101$
        
        $1010$

    - Sumem 1

        $1010 + 1 = 1011$
    
    Per tant, $Ca2 = Ca1 + 1$

- Convertir de binari negatiu a decimal
    - $+5 \rightarrow 0101$
    - $-5 \rightarrow 1011$
    - Sense tenir en compte el signe:
        - $1011 \rightarrow 8$ i el fem negatiu
        - $\,011 \rightarrow 3$
        - Per tant $-8+3 = -5$


## Els nombres reals


| Bit de signe | bit | ... | float point | ... | bit |
|--------------|-----|-----|-------------|-----|-----|

- **Qf** $\rightarrow$ *Q15* (15 bits per a la part fraccionària)
- **Qm.f** $\rightarrow$ *Q7.15* (7 bits per part entera i 15 bits per a la part fraccionària)
- **s : m : f** $\rightarrow$ *1:7:15* (1 bit per al signe, 7 bits per part entera i 15 bits per a la part fraccionària)

