# Dia 1

- Mon Feb 12

## Nivells d'un computador

1. Maquinari/Hardware
2. Llenguatge màquina (machine code)
3. Sistema operatiu (OS)
4. Llenguatges d'alt nivell (High-level languages)
5. Applicacions

## Estructura d'un computador

- Processador / CPU
- Memòria
- E/S (I/O)

<!-- English class >> Catalan class -->
## Architecture & Organization

- **Architectures** are those attributes visibles to the programmer.
- **Organizations** are how features are implemented.

- **Structures** are the way in which components relate to each other
- **Functions** are the operations of individual components as part of the structure.

___

## Nombres/Valors

### Calcul de valors a partir dels digits i bases

- 1992:
    - **d1:** 1
    - **d2:** 9
    - **d3:** 9
    - **d4:** 2

$$
\textrm{valor} = \sum_{p=0}^{\textrm{nº posicions} - 1} {d_p \cdot B^{p}}
$$
$$
B = \textrm{base}
$$

## Sistemes de numeració binnaria

- Base 2
- Bit: **Bi**nari dig**it**

## Conversions

### Decimal $\rightarrow$ binari

**23**

- 23 / 2 $\rightarrow$ Q: 11 **R: 1**
- 11 / 2 $\rightarrow$ Q: 5 **R: 1**
- 5 / 2 $\rightarrow$ Q: 2 **R: 1**
- 2 / 2 $\rightarrow$ Q: 1 **R: 0**
- 1 / 2 $\rightarrow$ Q: 0 **R: 1**

Per tant en binari és **10111**

### Binari $\rightarrow$ decimal

**10111**

- $d_0 \; \rightarrow$ 1
- $d_1 \; \rightarrow$ 1
- $d_2 \; \rightarrow$ 1
- $d_3 \; \rightarrow$ 0
- $d_4 \; \rightarrow$ 1

Decimal $\rightarrow \; 1 \cdot 2^4 + 0 \cdot 2^3 + 1 \cdot 2^2 + 1 \cdot 2^1 + 1 \cdot 2^0 = 23$
