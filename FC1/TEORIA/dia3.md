
# LLenguatge màquina

**Continguts:**

- Introducció a machine code ARM
- Introducció a assmembly (codi ensablador :sob:)
- Low level programming
- Rutines 


**Per a què?**

- Rutines de servei
- Gestió de sistemes operatius
- Embedded systems

## Introducció al llenguatge màquina

### El procesador

- Registres temporals
- Unitat aritmètica lògica
- Registres L.M
- Càlcul de direccions

**Arithmetic logic unit (ALU)**

- **32bit**
- Sumar
- Comparar
- AND, OR

### La memòria

- Cada direcció pot guardar un byte (8 bits)
- No es pot accedit a bits individuals
- No existeixen posicions buides
- El procesador llegeix la memòria
- El procesador pot sobreescriure a la memòria


## El procesador ARM

- 32 bits (Direccions)
- 8 bits (Continguts)
- 3 bits (ALU)

---

- Byte (8 bits)
- Halfword (16 bits)
- World (32 bits)

---

## El lleguatge màquina

### Els registres L.M.

- El ARM v4 té 16 registres (R0 a R15)
- Del R0 fins al R12 $\rightarrow$ registres de dades
- R13 $\rightarrow$ Stack pointer
- R14 $\rightarrow$ Link register
- R15 $\rightarrow$ 


## Assembly

### add (add)

```
add r0, r1, r2 

add r0, r1
```

### mov (move)

```
mov r0, r1
```

### cmp (compare)

```
cmp r0, r1
```

